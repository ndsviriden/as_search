## Motivation
This is the test case study task.

## Task
To rank the search results better, we want to predict possible categories to boost for a given user search query.
The task is to develop a simple CLI to predict categories to boost for a given search term.

## Data
`data/events.log` is the file containing `search` and `productview` events.  
The file is in JSONL format. Every row has an event serialized as json with the properties:
```
{
    "session": session id of the request,
    "name": name of the event,
    "query": (search event only) text the user searched,
    "title": (productview event only) title of the product,
    "product_id": (productview event only) id of the product,
    "category":(productview event only) category of the product,
}
```

`data/input.log` is JSONL file in the same format as `data/events.log`, with the only difference,  
that there are no produc tview related rows. This is the example file you can use as a template.

`data/output.log` is JSONL file in the same format as `data/input.log` with predicted categories. This is the template of the output file.  
You can produce it running all commands in Quick Start section with default arguments.

## Quick Start

1. Clone the project repository.
2. Place the file you wish to predict in the data/ folder. For example, you can create data/your_input.log using data/input.log as a template.
3. Build the Docker image by running `make build`.
4. Train the model by executing `make train`. As a result the classifier will be stored in models/classifier.pkl.
5. Run the prediction by executing 
`make predict input_data_path=data/your_input.log output_data_path=data/your_output.log`
If no arguments are provided (`make predict`), the default input file data/input.log will be used,
and the output will be stored in the default location data/output.log.
6. Find the output file in data/output.log `data/your_output.log`
