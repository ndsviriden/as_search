import argparse
import logging

from utils.data_loader import DataLoader
from utils.query_classifier import QueryClassifier

parser = argparse.ArgumentParser(description="Train models")
parser.add_argument("--train_data_path", type=str, help="Path to the training")
parser.add_argument("--classifier_path", type=str, help="Path to output file")


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


if __name__ == "__main__":

    args = parser.parse_args()

    # Load training data
    data_loader = DataLoader(
        train_data_path=args.train_data_path, classifier_path=args.classifier_path
    )
    views, queries = data_loader.read_train_data()

    # Training classifier
    classifier = QueryClassifier()
    classifier.fit(views, queries)

    # Saving classifier
    data_loader.save_classifier(classifier=classifier)
