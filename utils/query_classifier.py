import logging

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


class QueryClassifier:
    def __init__(self):
        self.bow = None
        self.lr = None

    @staticmethod
    def preprocess_data(views, queries):
        dataset = pd.merge(
            queries,
            views.groupby("search_id", as_index=False)["category"].first(),
            on="search_id",
        )
        dataset["category"] = (
            dataset.groupby("query")["category"]
            .transform(lambda x: " & ".join((set(x))))
            .values
        )
        X = dataset["query"].values
        y = dataset["category"]
        return X, y

    def fit(self, views, queries):
        X, y = self.preprocess_data(views, queries)

        self.bow = CountVectorizer(ngram_range=(1, 1))
        self.lr = LogisticRegression(multi_class="ovr")

        X = self.bow.fit_transform(X)
        self.lr.fit(X, y)

    def predict(self, X):
        X = self.bow.transform(X["query"].values)
        return self.lr.predict(X)
