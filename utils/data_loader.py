import pickle

import pandas as pd


class DataLoader:
    def __init__(
        self,
        train_data_path=None,
        input_data_path=None,
        output_data_path=None,
        classifier_path=None,
    ):
        self.train_data_path = train_data_path
        self.input_data_path = input_data_path
        self.output_data_path = output_data_path
        self.classifier_path = classifier_path

    def read_train_data(self):
        events = pd.read_json(
            self.train_data_path, lines=True, dtype={"product_id": "int64"}
        )

        events = events.rename(columns={"session": "session_id"})
        events["search_id"] = (events["name"] == "search").cumsum()

        view_columns = ["session_id", "search_id", "title", "product_id", "category"]
        views = events.loc[events["name"] == "productview", view_columns]

        query_columns = ["session_id", "search_id", "query"]
        queries = events.loc[events["name"] == "search", query_columns]
        return views, queries

    def read_input_data(self):
        events = pd.read_json(
            self.input_data_path, lines=True, dtype={"product_id": "int64"}
        )
        queries = events[events["name"] == "search"]
        return queries

    def save_output_data(self, data):
        with open(self.output_data_path, "w") as f:
            f.write(data.to_json(orient="records", lines=True, force_ascii=False))

    def read_classifier(self):

        with open(self.classifier_path, "rb") as fp:
            classifier = pickle.load(fp)

        return classifier

    def save_classifier(self, classifier):
        pickle.dump(classifier, open(self.classifier_path, "wb"))
