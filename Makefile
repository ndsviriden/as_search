.PHONY: build train

classifier_path ?= models/classifier.pkl
train_data_path ?= data/events.log
input_data_path ?= data/input.log
output_data_path ?= data/output.log

build:
	docker build \
		--platform=linux/amd64 \
		-t as_search .

train:
	docker run \
		-v $(shell pwd)/models:/app/models:rw \
		-v $(shell pwd)/data:/app/data:rw \
		as_search train.py \
		--train_data_path $(train_data_path) \
		--classifier_path $(classifier_path)

predict:
	docker run \
		-v $(shell pwd)/models:/app/models:rw \
		-v $(shell pwd)/data:/app/data:rw \
		as_search predict.py \
		--input_data_path $(input_data_path) \
		--output_data_path $(output_data_path) \
		--classifier_path $(classifier_path)