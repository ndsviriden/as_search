import argparse
import logging

from utils.data_loader import DataLoader

parser = argparse.ArgumentParser(description="Train models")
parser.add_argument("--input_data_path", type=str, help="Path to the input")
parser.add_argument("--output_data_path", type=str, help="Path to output file")
parser.add_argument("--classifier_path", type=str, help="Path to output file")


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

if __name__ == "__main__":

    args = parser.parse_args()

    # Load data and the classifier
    data_loader = DataLoader(
        input_data_path=args.input_data_path,
        output_data_path=args.output_data_path,
        classifier_path=args.classifier_path,
    )
    data = data_loader.read_input_data()
    classifier = data_loader.read_classifier()

    # Predict categories
    data["category"] = classifier.predict(data)

    # Save output
    data_loader.save_output_data(data)
